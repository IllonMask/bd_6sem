﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace ADO2
{
    public class Tipography
    {
        public String code { get; set; }
        public String name { get; set; }
        public String address { get; set; }
        public decimal telephone { get; set; }
        public Tipography(String code, String name, String address, decimal telephone)
        {
            this.code = code;
            this.name = name;
            this.address = address;
            this.telephone = telephone;
        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome, you should work with Typography table.");
            using (SqlConnection con = new SqlConnection("Server=DESKTOP-BK605PH\\SQLEXPRESS;Database=BD_Lab4;User Id=Sergey;Password=12345678;"))
            {
                con.Open();
                while (true)
                   {
                    String statement = Console.ReadLine();
                    switch (statement)
                    {
                        case "insert":
                            Console.WriteLine("You are choose insert procedure. Please enter Typography data.");
                            using (SqlCommand cmd = new SqlCommand("ins_Typography", con))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@code", Console.ReadLine());
                                cmd.Parameters.AddWithValue("@name", Console.ReadLine());
                                cmd.Parameters.AddWithValue("@adress", Console.ReadLine());
                                cmd.Parameters.AddWithValue("@telephone", Convert.ToInt64(Console.ReadLine()));
                                
                                cmd.ExecuteNonQuery();
                            }
                            Console.WriteLine("Insert complete!");
                            break;
                        case "update":
                            Console.WriteLine("You are choose update procedure. Please enter Typography data.");
                            using (SqlCommand cmd = new SqlCommand("upd_Typography", con))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@code", Console.ReadLine());
                                cmd.Parameters.AddWithValue("@name", Console.ReadLine());
                                cmd.Parameters.AddWithValue("@adress", Console.ReadLine());
                                cmd.Parameters.AddWithValue("@telephone", Convert.ToInt32(Console.ReadLine()));
                                cmd.ExecuteNonQuery();
                            }
                            Console.WriteLine("Update complete!");
                            break;
                        case "delete":
                            Console.WriteLine("You are choose delete procedure. Please enter Typography code.");
                            using (SqlCommand cmd = new SqlCommand("del_Typography", con))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@code", Console.ReadLine());
                                cmd.ExecuteNonQuery();
                            }
                            Console.WriteLine("Delete complete!");
                            break;
                        case "select":
                            Console.WriteLine("You are choose select.");
                            SqlCommand command = new SqlCommand("USE Laba1_4var;SELECT * FROM Typography;", con);
                            try
                            {
                                SqlDataReader reader = command.ExecuteReader();
                                List<Tipography> tipographies = new List<Tipography>();
                                while (reader.Read())
                                {
                                    Tipography tip = new Tipography(Convert.ToString(reader[0]), Convert.ToString(reader[1]), Convert.ToString(reader[2]), Convert.ToDecimal(Convert.ToString(reader[3])));
                                    tipographies.Add(tip);
                                }
                                foreach(Tipography tip in tipographies)
                                {
                                    Console.WriteLine(tip.code + ' ' + tip.name + ' ' + tip.telephone + ' ' + tip.address + '\n');
                                }
                                reader.Close();
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                            break;
                        case "function":
                            Console.WriteLine("You are choose select procedure. Please enter Typography code.");
                            using (var comman = con.CreateCommand())
                            {
                                comman.CommandType = CommandType.StoredProcedure;
                                comman.CommandText = "getOrderAmountByPeriod";
                                comman.Parameters.AddWithValue("@startDate", DateTime.Today.AddDays(1));
                                comman.Parameters.AddWithValue("@endDate", DateTime.Today.AddDays(2));

                                SqlParameter returnValue = comman.Parameters.Add("@retval", SqlDbType.Decimal);

                                returnValue.Direction = ParameterDirection.ReturnValue;
                                var q = comman.ExecuteScalar();
                                Console.WriteLine(returnValue.Value);
                                
                            }
                            Console.WriteLine("Func executed!");
                            break;
                        case "getSetIntersection":
                            Console.WriteLine("You are choose Intersection procedure. Please enter two ids.");
                            using (var comman = con.CreateCommand())
                            {
                                comman.CommandType = CommandType.StoredProcedure;
                                comman.CommandText = "getSetIntersection";
                                comman.Parameters.AddWithValue("@firstRectangle", Console.ReadLine());
                                comman.Parameters.AddWithValue("@secondRectangle", Console.ReadLine());


                                SqlParameter returnValue = comman.Parameters.Add("@ans", SqlDbType.NVarChar);

                                returnValue.Direction = ParameterDirection.ReturnValue;

                                comman.ExecuteNonQuery();
                                Object email = comman.Parameters["@ans"].Value;
                                Console.WriteLine(email.ToString());

                            }
                            Console.WriteLine("Func executed!");
                            break;
                        case "getSetUnion":
                            Console.WriteLine("You are choose select procedure. Please enter Typography code.");
                            using (var comman = con.CreateCommand())
                            {
                                comman.CommandType = CommandType.StoredProcedure;
                                comman.CommandText = "getSetUnion";
                                comman.Parameters.AddWithValue("@firstRectangle", Console.ReadLine());
                                comman.Parameters.AddWithValue("@secondRectangle", Console.ReadLine());


                                SqlParameter returnValue = comman.Parameters.Add("@ans", SqlDbType.NVarChar);

                                returnValue.Direction = ParameterDirection.ReturnValue;

                                comman.ExecuteNonQuery();
                                Object email = comman.Parameters["@ans"].Value;
                                Console.WriteLine(email.ToString());
                            }
                            Console.WriteLine("Func executed!");
                            break;
                        case "getSetDistance":
                            Console.WriteLine("You are choose select procedure. Please enter Typography code.");
                            using (var comman = con.CreateCommand())
                            {
                                comman.CommandType = CommandType.StoredProcedure;
                                comman.CommandText = "getSetDistance";
                                comman.Parameters.AddWithValue("@firstRectangle", Console.ReadLine());
                                comman.Parameters.AddWithValue("@secondRectangle", Console.ReadLine());


                                SqlParameter returnValue = comman.Parameters.Add("@ans", SqlDbType.Float);

                                returnValue.Direction = ParameterDirection.ReturnValue;

                                comman.ExecuteNonQuery();
                                Object email = comman.Parameters["@ans"].Value;
                                Console.WriteLine(email.ToString());
                            }
                            Console.WriteLine("Func executed!");
                            break;
                        case "getSetException":
                            Console.WriteLine("You are choose select procedure. Please enter Typography code.");
                            using (var comman = con.CreateCommand())
                            {
                                comman.CommandType = CommandType.StoredProcedure;
                                comman.CommandText = "getSetException";
                                comman.Parameters.AddWithValue("@firstRectangle", Console.ReadLine());
                                comman.Parameters.AddWithValue("@secondRectangle", Console.ReadLine());


                                SqlParameter returnValue = comman.Parameters.Add("@ans", SqlDbType.Float);

                                returnValue.Direction = ParameterDirection.ReturnValue;

                                comman.ExecuteNonQuery();
                                Object email = comman.Parameters["@ans"].Value;
                                Console.WriteLine(email.ToString());
                            }
                            Console.WriteLine("Func executed!");
                            break;
                        default:
                        Console.WriteLine("Please choose correct statement!");
                        break;
                    }
                }
            }
        }
    }
}
